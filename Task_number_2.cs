﻿using System;
using System.Collections.Generic;

namespace APPO;

public class Task_number_2 // Задача посчитать кол-во чисел, меньших 20, квадрат которых кратен 8 (отбросим отрицательную часть)
{
    private List<int> resList;

    public delegate void PrintRes(List<int> numbers);// Делегат для вывода данных
    
    public Task_number_2(PrintRes print)
    {
        resList = new List<int>();
        for (int i = 1; i < 20; i++)//Проходим по всем числам от 1 до 19
        {
            if (int.TryParse((Math.Pow(i, 2) / 8).ToString(), out int _))//Если результат вычисления является целочисленным записываем значение в список
            {
                resList.Add(i);
            }
        }
        print?.Invoke(resList);
    }
}