﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace APPO
{
    //Задача создать генератор чисел фибоначи, на входе длина последовательности
    public class Task_number_3
    {
        public delegate void PrintFib(List<int> fib);
        public PrintFib? print;
        public Task_number_3(int length, PrintFib func)
        {
            print = func;
            if (length <= 0)// Проверка, если пользователь ввел длину меньше 0 или равной 1 
                return;
            if (length == 1)
            {
                print?.Invoke(new List<int>(1));
                return;
            }
            List<int> fib = new List<int>(new []{ 1, 1 });// создаем список с исходными значениями последовательности
            while (fib.Count <= length)//Выполняем пока последовательность меньше заданного значения
            {
                int sum = 0;//переменная, куда будем записывать сумму последних двух элементов
                sum = fib.Last() + fib[fib.Count - 2];
                fib.Add(sum);//добавляем сумму в последовательность
            }
            print?.Invoke(fib);//Делегируем вывод данных
        }
    }
}