﻿using System;

namespace APPO;
 //Задача про вычисление выражения при заданном значении переменных
public class Task_number_1
{

    public delegate void PrintCalc(double res);// Делегат для вывода данных
    
    public double res { get; set; }//Свойство, в котором будет храниться итог вычисления 
    public PrintCalc print;
    public Task_number_1( PrintCalc prcl, double x = 5, double y = 7)
    {
        print = prcl;
        res = Math.Pow(Math.E, (x * x +y * y - x * y)) * Math.Cos(Math.Pow(x,y))/Math.Log10(Math.Pow(y,x));// Проводим вычисления
        print?.Invoke(res);//Выводим на экран
    }
}