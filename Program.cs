﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace APPO
{ 
    class Program
    {
        static void PrintFibonachi(List<int> fib) // Функция для делигирования вывода числе фибоначи
        {
            Console.WriteLine("\n=============================================");
            Console.Write("Результат задания 3: ");
            foreach (var VARIABLE in fib)
            {
                Console.Write(VARIABLE + " ");
            }
        }
        static void PrintCalc(double res) => Console.WriteLine($"Результат задания 1: {res.ToString()}");// Функция для делигирования вывода результата вычисления
        static void PrintList(List<int> numbers)// Функция для делигирования вывода кратных квадратов числу 8
        {
            Console.WriteLine("=============================================");
            Console.Write($"Результат задания 2: всего кратных чисел : {numbers.Count}, сами числа:");
            foreach (var VARIABLE in numbers)
            {
                Console.Write(VARIABLE + " ");
            }
        }// Функция для делигирования вывода
        static void PrintDict(Dictionary<string, uint> dict)
        {
            Console.WriteLine("\n=============================================");
            Console.WriteLine("Результат задания 4:");
            foreach (var VARIABLE in dict)//Выводим все содержимое словаря
            {
                Console.Write($"Key: {VARIABLE.Key}, Value: {VARIABLE.Value.ToString()}\n");
            }
        }// Функция для делигирования вывода объединенного словаря

        public static void Main(string[] args)
        { 
            new Task_number_1(PrintCalc);
            new Task_number_2(PrintList);
            new Task_number_3(5, PrintFibonachi);
            new Task_number_4(PrintDict);

        }
        
    }
}