﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace APPO;

public class Task_number_4 // Задача состоит в объединении двух словарей с заданием им уникальности для ключей исходных словарей.
{
    public delegate void PrintDict(Dictionary<string, uint> dict);//Делегат для вывода данных
    private PrintDict? print;
    private readonly Dictionary<string, uint> dict1 = new Dictionary<string, uint>()//Первый словарь
    {
        {"aaa", 5},
        {"bbb", 3},
        {"ccc", 2},
    };
    private readonly Dictionary<string, uint> dict2 = new Dictionary<string, uint>()//Второй словарь
    {
        {"fff", 5},
        {"eee", 3},
        {"ccc", 4},
    };
    
    private Dictionary<string, uint> res= new Dictionary<string, uint>();//Словарь - результат их объединения

    public Task_number_4(PrintDict print)
    {
        this.print = print; 
        res = dict1.Union(dict2).GroupBy(x => x.Key).ToDictionary(x=> x.Key, x=> (uint)x.Sum(k=>k.Value)); // Проводим все преобразования( если ключи одинаковы, то значение будет равно сумме значений у ключей
        this.print?.Invoke(res);//Передаем резульат на вывод
    }

}